﻿using UnityEngine;
using System.Collections;

public class AnalogClock : MonoBehaviour
{


    private MyTimer tiempo;
    public RectTransform manecilla;

	// Use this for initialization
	void Start ()
	{
	    tiempo = gameObject.GetComponent<MyTimer>();
        tiempo.StartTimer(10);
        // . getComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update ()
	{
        Debug.Log(tiempo.remainingTime);
	    manecilla.transform.eulerAngles = new Vector3(0,0, -SecondsToDegree(tiempo.remainingTime));
	}

    private float SecondsToDegree(float currentSecs)
    {
        return currentSecs*6;
    }
}

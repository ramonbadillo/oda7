﻿public static class Constants
{
    public static readonly int Rows = 8;
    public static readonly int Columns = 8;
    public static readonly float AnimationDuration = 0.2f;

    public static readonly float MoveAnimationMinDuration = 0.05f;

    public static readonly float ExplosionDuration = 0.3f;

    public static readonly float WaitBeforePotentialMatchesCheck = 1.0f;
    public static readonly float OpacityAnimationFrameDelay = 0.05f;

    public static readonly int MinimumMatches = 3;
    public static readonly int MinimumMatchesForBonus = 5;

    public static readonly int Match3Score = 5;
    public static readonly int Match4Score = 10;
    public static readonly int Match5Score = 20;
    public static readonly int SubsequentMatchScore = 5;
}
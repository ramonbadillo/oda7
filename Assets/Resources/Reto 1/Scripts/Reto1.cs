﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;



public class Reto1 : MonoBehaviour
{
    //Agregar la camara al canvas scaler///////////////////////////////////////////////////////
    public readonly int scoreToWin = 30;
    public readonly float timeToLose = 61.0f;
    public readonly int maxNumberHelp = 1;
	public readonly float timeToBlink = 10.0f;

    private int currentNumberHelp = 0;
    private Text scoreText, timeText, failText;
    private MyTimer myTimer;

    public ShapesArray shapes;

    private int score;

    public readonly Vector2 BottomRight = new Vector2(-3.5f, -3.80f);
    public readonly Vector2 CandySize = new Vector2(1.1f, 1.1f);

    private GameState state = GameState.None;
    private GameObject hitGo = null;
    private Vector2[] SpawnPositions;

    private GameObject FichasPrefabContainerGameObject;
    private List<GameObject> CandyPrefabs = new List<GameObject>();

    private GameObject ExplosionsPrefabContainerGameObject;
    private List<GameObject> ExplosionPrefabs = new List<GameObject>();

    private GameObject BonusPrefabContainerGameObject;
    private List<GameObject> BonusPrefabs = new List<GameObject>();

    private IEnumerator CheckPotentialMatchesCoroutine;
    private IEnumerator AnimatePotentialMatchesCoroutine, CheckPotentialMatchesCoroutineAuto;

    private IEnumerable<GameObject> potentialMatches;

	private GameObject containerGameObject;
	private Image backgroundImage;
	private bool animStop = true;
    private bool startedGame = false;
    private Button clueButton, pauseButton;

    private GameObject Reto1GameObject;

    private GameObject popUpElements, pausePopUp, failPopUp, winPopUp;
    private Button exitButtonPause, continueButtonPause, exitButtonFail, restartButtonFail, winButton, noMoreMatchesButton,noMoreHelpButton;

    private SoundManager soundManager;

    // Use this for initialization
    private void Start()
    {
        myTimer = gameObject.GetComponent<MyTimer>();
        soundManager = transform.GetComponent<SoundManager>();

		backgroundImage = Menu.Reto1GameObject.GetComponent<Image>();
        Reto1GameObject = Menu.Reto1GameObject;
        FichasPrefabContainerGameObject = Reto1GameObject.transform.GetChild(5).gameObject;
        ExplosionsPrefabContainerGameObject = Reto1GameObject.transform.GetChild(7).gameObject;
        BonusPrefabContainerGameObject = Reto1GameObject.transform.GetChild(6).gameObject;
        containerGameObject = Reto1GameObject.transform.GetChild(4).gameObject;

        timeText = Reto1GameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        scoreText = Reto1GameObject.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Text>();
        clueButton = Reto1GameObject.transform.GetChild(2).gameObject.GetComponent<Button>();
        pauseButton = Reto1GameObject.transform.GetChild(3).gameObject.GetComponent<Button>();
        popUpElements = Reto1GameObject.transform.GetChild(8).gameObject;
        pausePopUp = popUpElements.transform.GetChild(0).gameObject;
        exitButtonPause = pausePopUp.transform.GetChild(0).GetComponent<Button>();
        continueButtonPause = pausePopUp.transform.GetChild(1).GetComponent<Button>();
        failPopUp = popUpElements.transform.GetChild(1).gameObject;
        restartButtonFail = failPopUp.transform.GetChild(0).GetComponent<Button>();
        exitButtonFail = failPopUp.transform.GetChild(1).GetComponent<Button>();
        failText = failPopUp.transform.GetChild(2).GetComponent<Text>();
        winButton = popUpElements.transform.GetChild(2).gameObject.GetComponent<Button>();
        noMoreMatchesButton = popUpElements.transform.GetChild(3).GetComponent<Button>();
        noMoreHelpButton = popUpElements.transform.GetChild(3).GetComponent<Button>();
        winPopUp = popUpElements.transform.GetChild(2).gameObject;

        noMoreHelpButton.onClick.AddListener(delegate ()
         {
             noMoreHelpButton.gameObject.SetActive(false);
         });

        noMoreMatchesButton.onClick.AddListener(delegate ()
        {
            noMoreMatches();
        });

        winButton.onClick.AddListener(delegate ()
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto2);
            gameObject.GetComponent<Reto2>().StartReto();
            Time.timeScale = 1.0f;
        });

        restartButtonFail.onClick.AddListener(delegate ()
        {
            soundManager.playSound("Sound_Cerrar 3", 1);
            StartReto();
        });

        exitButtonFail.onClick.AddListener(delegate ()
        {
            ExitGame();
        });

        exitButtonPause.onClick.AddListener(delegate ()
        {
            ExitGame();
        });

        continueButtonPause.onClick.AddListener(delegate ()
        {
            ResumeGame();
        });

        pauseButton.onClick.AddListener(delegate ()
        {
            PauseGame();
        });
        clueButton.onClick.AddListener(delegate ()
        {
            StartCheckForPotentialMatches(0.03f);
        });

        InitializeTypesOnPrefabShapesAndBonuses();
        //StartReto();
    }

    private void noMoreMatches()
    {
        popUpElements.SetActive(false);
        noMoreMatchesButton.gameObject.SetActive(false);
        InitializeCandyAndSpawnPositions();
        Time.timeScale = 1.0f;
    }

    private void ClearReto()
    {
        foreach (Transform currentPopUp in popUpElements.transform)
        {
            currentPopUp.gameObject.SetActive(false);
        }
        popUpElements.SetActive(false);
        Menu.DropDownGameObject.SetActive(true);
    }

    public void StartReto()
    {
		animStop = true;
		backgroundImage.GetComponent<Animator>().Play("default");
        state = GameState.None;
        ClearReto();
        startedGame = true;
        InitializeVariables();
        Time.timeScale = 1.0f;
        myTimer.ResetMyTimer();
        myTimer.StartTimer(timeToLose);
        StartCoroutine(WaitForIt());
    }

    private void ResumeGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        Time.timeScale = 1.0f;
        startedGame = true;
        popUpElements.SetActive(false);
        pausePopUp.SetActive(false);
    }

    private void ExitGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        Menu.DropDownGameObject.SetActive(true);
        Time.timeScale = 1.0f;
    }

    private void PauseGame()
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        Time.timeScale = 0.0f;
        startedGame = false;
        popUpElements.SetActive(true);
        pausePopUp.SetActive(true);
    }

    private void LoseGame()
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        StopCheckForPotentialMatches();
        failText.text = "Excelente, conseguiste " + score + " puntos.";
        Time.timeScale = 0.0f;
        Menu.DropDownGameObject.SetActive(false);

        popUpElements.SetActive(true);
        failPopUp.SetActive(true);
        startedGame = false;
    }

    private void WinGame()
    {
        soundManager.playSound("TerminarReto", 1);
        popUpElements.SetActive(true);
        winPopUp.SetActive(true);
        //Time.timeScale = 0.0f;
        StopCheckForPotentialMatches();
        myTimer.StopTimer();
        startedGame = false;
    }

    private IEnumerator WaitForIt()
    {
        yield return new WaitForSeconds(0.1f);
        InitializeCandyAndSpawnPositions();
    }

    /// <summary>
    /// Initialize shapes
    /// </summary>
    private void InitializeTypesOnPrefabShapesAndBonuses()
    {
        foreach (Transform prefab in FichasPrefabContainerGameObject.transform)
        {
            prefab.gameObject.AddComponent<Shape>();
            
            CandyPrefabs.Add(prefab.gameObject);
        }

        foreach (Transform prefab in BonusPrefabContainerGameObject.transform)
        {
            prefab.gameObject.AddComponent<Shape>();
            BonusPrefabs.Add(prefab.gameObject);
        }

        foreach (Transform prefab in ExplosionsPrefabContainerGameObject.transform)
        {
            prefab.gameObject.AddComponent<Shape>();
            ExplosionPrefabs.Add(prefab.gameObject);
        }

        //just assign the name of the prefab
        foreach (var item in CandyPrefabs)
        {
            item.GetComponent<Shape>().Type = item.name;
        }

        //assign the name of the respective "normal" candy as the type of the Bonus
        foreach (var item in BonusPrefabs)
        {
            item.GetComponent<Shape>().Type = CandyPrefabs.
                Where(x => x.GetComponent<Shape>().Type.Contains(item.name.Split('_')[1].Trim())).Single().name;
        }
    }

    public void InitializeCandyAndSpawnPositionsFromPremadeLevel()
    {
        InitializeVariables();

        var premadeLevel = DebugUtilities.FillShapesArrayFromResourcesData();

        if (shapes != null)
            DestroyAllCandy();

        shapes = new ShapesArray();
        SpawnPositions = new Vector2[Constants.Columns];

        for (int row = 0; row < Constants.Rows; row++)
        {
            for (int column = 0; column < Constants.Columns; column++)
            {
                GameObject newCandy = null;

                newCandy = GetSpecificCandyOrBonusForPremadeLevel(premadeLevel[row, column]);

                InstantiateAndPlaceNewCandy(row, column, newCandy);
            }
        }

        SetupSpawnPositions();
    }

    public void InitializeCandyAndSpawnPositions()
    {
        if (shapes != null)
            DestroyAllCandy();

        shapes = new ShapesArray();
        SpawnPositions = new Vector2[Constants.Columns];

        for (int row = 0; row < Constants.Rows; row++)
        {
            for (int column = 0; column < Constants.Columns; column++)
            {
                GameObject newCandy = GetRandomCandy();

                //check if two previous horizontal are of the same type
                while (column >= 2 && shapes[row, column - 1].GetComponent<Shape>()
                    .IsSameType(newCandy.GetComponent<Shape>())
                    && shapes[row, column - 2].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>()))
                {
                    newCandy = GetRandomCandy();
                }

                //check if two previous vertical are of the same type
                while (row >= 2 && shapes[row - 1, column].GetComponent<Shape>()
                    .IsSameType(newCandy.GetComponent<Shape>())
                    && shapes[row - 2, column].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>()))
                {
                    newCandy = GetRandomCandy();
                }

                InstantiateAndPlaceNewCandy(row, column, newCandy);
            }
        }

        SetupSpawnPositions();
    }

    private void InstantiateAndPlaceNewCandy(int row, int column, GameObject newCandy)
    {
        GameObject go = Instantiate(newCandy,
            BottomRight + new Vector2(column * CandySize.x, row * CandySize.y), Quaternion.identity)
            as GameObject;

        go.transform.SetParent(containerGameObject.transform);

        //assign the specific properties
        go.GetComponent<Shape>().Assign(newCandy.GetComponent<Shape>().Type, row, column);
        shapes[row, column] = go;
    }

    private void SetupSpawnPositions()
    {
        //create the spawn positions for the new shapes (will pop from the 'ceiling')
        for (int column = 0; column < Constants.Columns; column++)
        {
            SpawnPositions[column] = BottomRight
                + new Vector2(column * CandySize.x, Constants.Rows * CandySize.y);
        }
    }

    /// <summary>
    /// Destroy all candy gameobjects
    /// </summary>
    private void DestroyAllCandy()
    {
        for (int row = 0; row < Constants.Rows; row++)
        {
            for (int column = 0; column < Constants.Columns; column++)
            {
                Destroy(shapes[row, column]);
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (startedGame)
        {
            timeText.text = myTimer.timerText;

            if (state == GameState.None)
            {
                //user has clicked or touched
                if (Input.GetMouseButtonDown(0))
                {
                    //get the hit position
                    var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                    if (hit.collider != null) //we have a hit!!!
                    {
                        hitGo = hit.collider.gameObject;
                        state = GameState.SelectionStarted;
                    }
                }
            }
            else if (state == GameState.SelectionStarted)
            {
                //user dragged
                if (Input.GetMouseButton(0))
                {
                    var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                    //we have a hit
                    if (hit.collider != null && hitGo != hit.collider.gameObject)
                    {
                        //user did a hit, no need to show him hints
                        StopCheckForPotentialMatches();

                        //if the two shapes are diagonally aligned (different row and column), just return
                        if (!Utilities.AreVerticalOrHorizontalNeighbors(hitGo.GetComponent<Shape>(),
                            hit.collider.gameObject.GetComponent<Shape>()))
                        {
                            state = GameState.None;
                        }
                        else
                        {
                            state = GameState.Animating;
                            FixSortingLayer(hitGo, hit.collider.gameObject);
                            StartCoroutine(FindMatchesAndCollapse(hit));
                        }
                    }
                }
            }

			if (myTimer.remainingTime <= timeToBlink && animStop)
			{
				animStop = false;
				backgroundImage.GetComponent<Animator>().Play("BackgroundAnim");
			}
            if (myTimer.remainingTime <= 0.0f)
            {
                LoseGame();
                startedGame = false;
            }
        }
    }

    /// <summary>
    /// Modifies sorting layers for better appearance when dragging/animating
    /// </summary>
    /// <param name="hitGo"></param>
    /// <param name="hitGo2"></param>
    private void FixSortingLayer(GameObject hitGo, GameObject hitGo2)
    {
        SpriteRenderer sp1 = hitGo.GetComponent<SpriteRenderer>();
        SpriteRenderer sp2 = hitGo2.GetComponent<SpriteRenderer>();
        if (sp1.sortingOrder <= sp2.sortingOrder)
        {
            sp1.sortingOrder = 1;
            sp2.sortingOrder = 0;
        }
    }

    private IEnumerator FindMatchesAndCollapse(RaycastHit2D hit2)
    {
        //get the second item that was part of the swipe
        var hitGo2 = hit2.collider.gameObject;
        shapes.Swap(hitGo, hitGo2);

        Vector3 oldPosition = hitGo.transform.position;

        //move the swapped ones
        //hitGo.transform.position= hitGo2.transform.position;
        //hitGo2.transform.position= oldPosition;

        hitGo.transform.positionTo(Constants.AnimationDuration, hitGo2.transform.position);
        hitGo2.transform.positionTo(Constants.AnimationDuration, hitGo.transform.position);
        yield return new WaitForSeconds(Constants.AnimationDuration);

        //get the matches via the helper methods
        var hitGomatchesInfo = shapes.GetMatches(hitGo);
        var hitGo2matchesInfo = shapes.GetMatches(hitGo2);

        var totalMatches = hitGomatchesInfo.MatchedCandy
            .Union(hitGo2matchesInfo.MatchedCandy).Distinct();

        

        //if user's swap didn't create at least a 3-match, undo their swap
        if (totalMatches.Count() < Constants.MinimumMatches)
        {
            hitGo.transform.positionTo(Constants.AnimationDuration, hitGo2.transform.position);
            hitGo2.transform.positionTo(Constants.AnimationDuration, hitGo.transform.position);
            yield return new WaitForSeconds(Constants.AnimationDuration);

            shapes.UndoSwap();
        }

        //if more than 3 matches and no Bonus is contained in the line, we will award a new Bonus
        bool addBonus = totalMatches.Count() >= Constants.MinimumMatchesForBonus &&
            !BonusTypeUtilities.ContainsDestroyWholeRowColumn(hitGomatchesInfo.BonusesContained) &&
            !BonusTypeUtilities.ContainsDestroyWholeRowColumn(hitGo2matchesInfo.BonusesContained);

        Shape hitGoCache = null;
        if (addBonus)
        {
            //get the game object that was of the same type
            var sameTypeGo = hitGomatchesInfo.MatchedCandy.Count() > 0 ? hitGo : hitGo2;
            hitGoCache = sameTypeGo.GetComponent<Shape>();
        }

        int timesRun = 1;
        while (totalMatches.Count() >= Constants.MinimumMatches)
        {
            //increase score///////////////////////////////////////////////////////////////////////////////////////////
            if (totalMatches.Count() == 3)
            {
                soundManager.playSound("Copia de Sound_click",1);
                IncreaseScore(Constants.Match3Score);
            }
            else if (totalMatches.Count() == 4)
            {
                soundManager.playSound("Copia de Sound_click", 1);
                IncreaseScore(Constants.Match4Score);
            }
            else if (totalMatches.Count() == 5)
            {
                soundManager.playSound("Sound_SumarioRespuestas", 1);
                IncreaseScore(Constants.Match5Score);
            }
            //IncreaseScore((totalMatches.Count() - 2) * Constants.Match3Score);

            //if (timesRun >= 2)
            //   IncreaseScore(Constants.SubsequentMatchScore);

            //soundManager.PlayCrincle();

            foreach (var item in totalMatches)
            {
                shapes.Remove(item);
                RemoveFromScene(item);
            }

            //check and instantiate Bonus if needed
            if (addBonus)
                CreateBonus(hitGoCache);

            addBonus = false;

            //get the columns that we had a collapse
            var columns = totalMatches.Select(go => go.GetComponent<Shape>().Column).Distinct();

            //the order the 2 methods below get called is important!!!
            //collapse the ones gone
            var collapsedCandyInfo = shapes.Collapse(columns);
            //create new ones
            var newCandyInfo = CreateNewCandyInSpecificColumns(columns);

            int maxDistance = Mathf.Max(collapsedCandyInfo.MaxDistance, newCandyInfo.MaxDistance);

            MoveAndAnimate(newCandyInfo.AlteredCandy, maxDistance);
            MoveAndAnimate(collapsedCandyInfo.AlteredCandy, maxDistance);

            //will wait for both of the above animations
            yield return new WaitForSeconds(Constants.MoveAnimationMinDuration * maxDistance);

            //search if there are matches with the new/collapsed items
            totalMatches = shapes.GetMatches(collapsedCandyInfo.AlteredCandy).
                Union(shapes.GetMatches(newCandyInfo.AlteredCandy)).Distinct();

            timesRun++;
        }

        state = GameState.None;
        StartCheckForPotentialMatches();
    }

    /// <summary>
    /// Creates a new Bonus based on the shape parameter
    /// </summary>
    /// <param name="hitGoCache"></param>
    private void CreateBonus(Shape hitGoCache)
    {
        GameObject Bonus = Instantiate(GetBonusFromType(hitGoCache.Type), BottomRight
            + new Vector2(hitGoCache.Column * CandySize.x,
                hitGoCache.Row * CandySize.y), Quaternion.identity)
            as GameObject;
        Bonus.transform.SetParent(containerGameObject.transform);

        shapes[hitGoCache.Row, hitGoCache.Column] = Bonus;
        var BonusShape = Bonus.GetComponent<Shape>();
        //will have the same type as the "normal" candy
        BonusShape.Assign(hitGoCache.Type, hitGoCache.Row, hitGoCache.Column);
        //add the proper Bonus type
        BonusShape.Bonus |= BonusType.DestroyWholeRowColumn;
    }

    /// <summary>
    /// Spawns new candy in columns that have missing ones
    /// </summary>
    /// <param name="columnsWithMissingCandy"></param>
    /// <returns>Info about new candies created</returns>
    private AlteredCandyInfo CreateNewCandyInSpecificColumns(IEnumerable<int> columnsWithMissingCandy)
    {
        AlteredCandyInfo newCandyInfo = new AlteredCandyInfo();

        //find how many null values the column has
        foreach (int column in columnsWithMissingCandy)
        {
            var emptyItems = shapes.GetEmptyItemsOnColumn(column);
            foreach (var item in emptyItems)
            {
                var go = GetRandomCandy();
                GameObject newCandy = Instantiate(go, SpawnPositions[column], Quaternion.identity)
                    as GameObject;

                newCandy.transform.SetParent(containerGameObject.transform);

                newCandy.GetComponent<Shape>().Assign(go.GetComponent<Shape>().Type, item.Row, item.Column);

                if (Constants.Rows - item.Row > newCandyInfo.MaxDistance)
                    newCandyInfo.MaxDistance = Constants.Rows - item.Row;

                shapes[item.Row, item.Column] = newCandy;
                newCandyInfo.AddCandy(newCandy);
            }
        }
        return newCandyInfo;
    }

    /// <summary>
    /// Animates gameobjects to their new position
    /// </summary>
    /// <param name="movedGameObjects"></param>
    private void MoveAndAnimate(IEnumerable<GameObject> movedGameObjects, int distance)
    {
        foreach (var item in movedGameObjects)
        {
            item.transform.positionTo(Constants.MoveAnimationMinDuration * distance, BottomRight +
                new Vector2(item.GetComponent<Shape>().Column * CandySize.x, item.GetComponent<Shape>().Row * CandySize.y));
        }
    }

    /// <summary>
    /// Destroys the item from the scene and instantiates a new explosion gameobject
    /// </summary>
    /// <param name="item"></param>
    private void RemoveFromScene(GameObject item)
    {
        //GameObject explosion = GetRandomExplosion();//////////////////////////////////////////////////////////////////////////////////////
        //var newExplosion = Instantiate(explosion, item.transform.position, Quaternion.identity) as GameObject;
       //Destroy(newExplosion, Constants.ExplosionDuration);
        Destroy(item);
    }

    /// <summary>
    /// Get a random candy
    /// </summary>
    /// <returns></returns>
    private GameObject GetRandomCandy()
    {
        
        return CandyPrefabs[UnityEngine.Random.Range(0, CandyPrefabs.Count)];
    }

    private void InitializeVariables()
    {
        score = 0;
        ShowScore();
    }

    private void IncreaseScore(int amount)
    {
        score += amount;

        ShowScore();
        if (score >= scoreToWin)
        {
            WinGame();
        }
    }

    private void ShowScore()
    {
        scoreText.text = " " + score.ToString();
    }

    /// <summary>
    /// Get a random explosion
    /// </summary>
    /// <returns></returns>
    private GameObject GetRandomExplosion()
    {
        return ExplosionPrefabs[Random.Range(0, ExplosionPrefabs.Count)];
    }

    /// <summary>
    /// Gets the specified Bonus for the specific type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private GameObject GetBonusFromType(string type)
    {
        string color = type.Split('_')[1].Trim();
        foreach (var item in BonusPrefabs)
        {
            if (item.GetComponent<Shape>().Type.Contains(color))
                return item;
        }
        throw new System.Exception("Wrong type");
    }

    /// <summary>
    /// Starts the coroutines, keeping a reference to stop later
    /// </summary>
    private void StartCheckForPotentialMatches()
    {
        StopCheckForPotentialMatches();
        //get a reference to stop it later
        CheckPotentialMatchesCoroutineAuto = CheckPotentialMatches();
        StartCoroutine(CheckPotentialMatchesCoroutineAuto);
    }

    private void StartCheckForPotentialMatches(float time)
    {
        if (currentNumberHelp != maxNumberHelp)
        {
            StopCheckForPotentialMatches();
            //get a reference to stop it later
            CheckPotentialMatchesCoroutine = CheckPotentialMatches(time);
            StartCoroutine(CheckPotentialMatchesCoroutine);
            currentNumberHelp++;
        }
        else
        {
            noMoreHelpButton.gameObject.SetActive(true);
        }
        
    }

    /// <summary>
    /// Stops the coroutines
    /// </summary>
    private void StopCheckForPotentialMatches()
    {
        if (AnimatePotentialMatchesCoroutine != null)
            StopCoroutine(AnimatePotentialMatchesCoroutine);
        if (CheckPotentialMatchesCoroutine != null)
            StopCoroutine(CheckPotentialMatchesCoroutine);
        ResetOpacityOnPotentialMatches();
    }

    /// <summary>
    /// Resets the opacity on potential matches (probably user dragged something?)
    /// </summary>
    private void ResetOpacityOnPotentialMatches()
    {
        if (potentialMatches != null)
            foreach (var item in potentialMatches)
            {
                if (item == null) break;

                Color c = item.GetComponent<SpriteRenderer>().color;
                c.a = 1.0f;
                item.GetComponent<SpriteRenderer>().color = c;
            }
    }

    /// <summary>
    /// Finds potential matches
    /// </summary>
    /// <returns></returns>
    private IEnumerator CheckPotentialMatches(float time)
    {
        yield return new WaitForSeconds(time);
        potentialMatches = Utilities.GetPotentialMatches(shapes);
        if (potentialMatches != null)
        {
            while (true)
            {
                AnimatePotentialMatchesCoroutine = Utilities.AnimatePotentialMatches(potentialMatches);
                StartCoroutine(AnimatePotentialMatchesCoroutine);
                yield return new WaitForSeconds(Constants.WaitBeforePotentialMatchesCheck);
            }
        }
    }

    /// <summary>
    /// Finds potential matches
    /// </summary>
    /// <returns></returns>
    private IEnumerator CheckPotentialMatches()
    {
        yield return new WaitForSeconds(Constants.WaitBeforePotentialMatchesCheck);
        potentialMatches = Utilities.GetPotentialMatches(shapes);

        if (potentialMatches == null)
        {
            Time.timeScale = 0.0f;
            popUpElements.SetActive(true);
            state = GameState.None;
            noMoreMatchesButton.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Gets a specific candy or Bonus based on the premade level information.
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    private GameObject GetSpecificCandyOrBonusForPremadeLevel(string info)
    {
        var tokens = info.Split('_');

        if (tokens.Count() == 1)
        {
            foreach (var item in CandyPrefabs)
            {
                if (item.GetComponent<Shape>().Type.Contains(tokens[0].Trim()))
                    return item;
            }
        }
        else if (tokens.Count() == 2 && tokens[1].Trim() == "B")
        {
            foreach (var item in BonusPrefabs)
            {
                if (item.name.Contains(tokens[0].Trim()))
                    return item;
            }
        }

        throw new System.Exception("Wrong type, check your premade level");
        
    }
}












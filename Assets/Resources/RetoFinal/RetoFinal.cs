﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetoFinal : MonoBehaviour
{
    private List<Question> FullTest;
    private List<Question> Test = new List<Question>();
    private List<bool> answersReviewed = new List<bool>();
    private Button nextButton, backButton;

    //Question Elements
    private GameObject questionsContainerGameObject;

    private Text questionText;
    private Text questionNumberText;
    private int _questionIndex;
    private GameObject questionElementsGameObject;

    private GameObject secondQuestionGameObject,
        tirdhQuestionGameObject,
        fourthQuestionGameObject,
        fifthQuestionGameObject,
        sixthQuestionGameObject,
        seventhQuestionGameObject,
        eighthtQuestionGameObject;

    //OneAnswerElements
    private GameObject singleAnswerContainer;

    private Color answerSettedColor = Color.blue;
    private Color defaultAnswerColor = Color.black;

    //Results Elements
    private GameObject resultPrefabGameObject;

    private GameObject resultsContainer;
    private Text mainResultText;
    private Button salirRetoFinalButton;

    //Message Box Elements
    private GameObject yesNoMessageBoxGameObject;

    private Text yesNoMessageBoxText;
    private Button yesButton, noButton;

    private SoundManager soundManager;

    private void CreateRandomTest(int numberQuestions)
    {
        for (int i = 0; i < numberQuestions; i++)
        {
            int randomIndex = Random.Range(0, FullTest.Count);
            Test.Add(FullTest[randomIndex]);
            FullTest.RemoveAt(randomIndex);
        }
    }

    // Use this for initialization
    private void Start()
    {
        soundManager = transform.GetComponent<SoundManager>();
        questionsContainerGameObject = Menu.RetoFinalGameObject.transform.GetChild(0).gameObject;
        resultsContainer = Menu.RetoFinalGameObject.transform.GetChild(1).gameObject;
        salirRetoFinalButton = resultsContainer.transform.GetChild(1).gameObject.GetComponent<Button>();
        mainResultText = resultsContainer.transform.GetChild(2).gameObject.GetComponent<Text>();
        resultPrefabGameObject = resultsContainer.transform.GetChild(3).gameObject;

        backButton = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(0).GetComponent<Button>();
        nextButton = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(1).GetComponent<Button>();
        questionText = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(2).GetComponent<Text>();
        questionNumberText = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>();
        singleAnswerContainer = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(3).gameObject;

        questionElementsGameObject = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(4).gameObject;
        secondQuestionGameObject = questionElementsGameObject.transform.GetChild(0).gameObject;
        tirdhQuestionGameObject = questionElementsGameObject.transform.GetChild(1).gameObject;
        fourthQuestionGameObject = questionElementsGameObject.transform.GetChild(2).gameObject;
        fifthQuestionGameObject = questionElementsGameObject.transform.GetChild(3).gameObject;
        sixthQuestionGameObject = questionElementsGameObject.transform.GetChild(4).gameObject;
        seventhQuestionGameObject = questionElementsGameObject.transform.GetChild(5).gameObject;
        eighthtQuestionGameObject = questionElementsGameObject.transform.GetChild(6).gameObject;

        yesNoMessageBoxGameObject = Menu.RetoFinalGameObject.transform.GetChild(2).gameObject;
        yesButton = yesNoMessageBoxGameObject.transform.GetChild(1).GetComponent<Button>();
        noButton = yesNoMessageBoxGameObject.transform.GetChild(2).GetComponent<Button>();
        yesNoMessageBoxText = yesNoMessageBoxGameObject.transform.GetChild(3).GetComponent<Text>();

        nextButton.onClick.AddListener(delegate ()
        {
            NextQuestion();
        });
        backButton.onClick.AddListener(delegate ()
        {
            BackQuestion();
        });

        salirRetoFinalButton.onClick.AddListener(delegate ()
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        });

        yesButton.onClick.AddListener(delegate ()
         {
             YesMethod();
         });

        noButton.onClick.AddListener(delegate ()
         {
             NoMethod();
         });

        //StartReto();
    }

    private void ClearReto()
    {
        nextButton.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
        nextButton.GetComponent<RectTransform>().localPosition = new Vector3(0, -207);
        backButton.gameObject.SetActive(false);
        questionsContainerGameObject.SetActive(true);
        resultPrefabGameObject.SetActive(true);
        resultsContainer.SetActive(false);
    }

    public void StartReto()
    {
        soundManager.playSound("Sound_Fondoevaluacion", 0);
        _questionIndex = 0;
        FillTest();
        CreateRandomTest(5);
        foreach (Transform currentButtonGameObject in singleAnswerContainer.transform)
        {
            int buttonIndex = currentButtonGameObject.transform.GetSiblingIndex();
            currentButtonGameObject.gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
            {
                SetAnswer(buttonIndex, singleAnswerContainer);
            });
        }
        SetQuestions(0);
    }

    private void YesMethod()
    {
        if (_questionIndex <= 0)
        {
            yesNoMessageBoxGameObject.SetActive(false);
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        }
        else
        {
            yesNoMessageBoxGameObject.SetActive(false);
            EvaluateTest();
        }
    }

    private void NoMethod()
    {
        yesNoMessageBoxGameObject.SetActive(false);
    }

    private void ShowMessageBox(string Message)
    {
        yesNoMessageBoxText.text = Message;
        yesNoMessageBoxGameObject.SetActive(true);
    }

    private void SetAnswer(int answerIndex, GameObject answerContainerGameObject)
    {
        soundManager.playSound("Sound_SeleccionarRespuesta", 1);
        Test[_questionIndex].SetAnswer(answerIndex);
        foreach (Transform currentButtonGameObject in answerContainerGameObject.transform)
        {
            if (currentButtonGameObject.transform.GetSiblingIndex() == answerIndex)
            {
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().color = answerSettedColor;
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>().color = answerSettedColor;
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Image>().color = answerSettedColor;
            }
            else
            {
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().color = defaultAnswerColor;
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>().color = defaultAnswerColor;
                currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Image>().color = defaultAnswerColor;
            }
        }
    }


    private Vector3 nextButtonPosition = new Vector3(265, -207);

    private void NextQuestion()
    {
        if (_questionIndex >= Test.Count - 1)
        {
            questionsContainerGameObject.SetActive(false);
            EvaluateTest();
        }
        else
        {
            if (_questionIndex >= Test.Count - 2)
            {
                nextButton.transform.GetChild(0).GetComponent<Text>().text = "TERMINAR";
            }
            else
            {
                nextButton.GetComponent<RectTransform>().localPosition = nextButtonPosition;
                backButton.transform.GetChild(0).GetComponent<Text>().text = "ATRAS";
                backButton.gameObject.SetActive(true);
            }
            _questionIndex = Mathf.Clamp(_questionIndex + 1, 0, Test.Count - 1);

            SetQuestions(_questionIndex);
        }
    }

    private void BackQuestion()
    {
        if (_questionIndex <= 1)
        {
            backButton.transform.GetChild(0).GetComponent<Text>().text = "INICIO";
            nextButton.GetComponent<RectTransform>().localPosition = new Vector3(0, -207);
            backButton.gameObject.SetActive(false);
        }
        else
        {
            backButton.gameObject.SetActive(true);
            nextButton.GetComponent<RectTransform>().localPosition = nextButtonPosition;
            nextButton.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
        }

        _questionIndex = Mathf.Clamp(_questionIndex - 1, 0, Test.Count - 1);
        SetQuestions(_questionIndex);
    }

    private void SetQuestions(int questionIndex)
    {
        foreach (Transform currentGameObject in questionElementsGameObject.transform)
        {
            currentGameObject.gameObject.SetActive(false);
        }

        questionNumberText.text = questionIndex + 1 + ".";
        questionText.text = Test[questionIndex].GetQuestionText();
        Test[questionIndex].SetActiveAnotherGameObjects(true);
        GameObject answerContainerGameObject = Test[questionIndex].GetAnswerContainer();
        int indexAnswer = 0;

        foreach (string answerString in Test[questionIndex].GetAnserTexts())
        {
            ToFraction(answerString, answerContainerGameObject.transform.GetChild(indexAnswer).gameObject);
            indexAnswer++;
        }

        if (Test[questionIndex].GetCurrentAnswers().Count > 0)
        {
            SetAnswer(Test[questionIndex].GetCurrentAnswers()[0], answerContainerGameObject);
        }
        else
        {
            CleanAnswers(answerContainerGameObject);
        }
    }

    private void CleanAnswers(GameObject answerContainerGameObject)
    {
        foreach (Transform currentButtonGameObject in answerContainerGameObject.transform)
        {
            currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().color = defaultAnswerColor;
            currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>().color = defaultAnswerColor;
            currentButtonGameObject.gameObject.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Image>().color = defaultAnswerColor;
        }
    }

    private void EvaluateTest()
    {
        GameObject resultGameObject;
        int questionPositionIndex = 0;
        int rigthQuestions = 0;
        foreach (Question currentQuestion in Test)
        {
            answersReviewed.Add(currentQuestion.ReviewQuestion());
            resultGameObject = (GameObject)Instantiate(resultPrefabGameObject);

            resultGameObject.transform.SetParent(resultsContainer.transform);
            resultGameObject.GetComponent<RectTransform>().localPosition = resultPrefabGameObject.transform.position + (Vector3.down * -160) + ((Vector3.down) * questionPositionIndex * 40);
            resultGameObject.transform.localScale = new Vector3(1, 1, 1);
            resultGameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = (questionPositionIndex + 1) + ".";
            if (currentQuestion.ReviewQuestion())
            {
                resultGameObject.GetComponent<Text>().text = "Correcto";
                resultGameObject.GetComponent<Text>().color = Color.green;
                rigthQuestions++;
            }

            questionPositionIndex++;
        }

        mainResultText.text = "CALIFICACIÓN : " + rigthQuestions * 2 + "/" + Test.Count * 2;
        resultPrefabGameObject.SetActive(false);
        resultsContainer.SetActive(true);
    }

    private void ToFraction(string fraction, GameObject fractionGameObject)
    {
        string[] numbers = fraction.Split('/');
        fractionGameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "" + numbers[0];
        fractionGameObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "" + numbers[1];
    }

    private void FillTest()
    {
        FullTest = new List<Question>();
        FullTest.Clear();
        Test.Clear();
        answersReviewed.Clear();
        FullTest.Add(new Question("Clara se compró un panqué que viene dividido en 9 pedazos. Al llegar a casa compartió el pan con sus tres primos: Felipe se comió 1 pedazo, Julieta 3 y Jimena 3.  ¿Qué fracción representa la cantidad de pan que le quedó a Clara?",
                               new string[] { "7/9", "2/9", "1/9" },
                               null,
                               singleAnswerContainer,
                               1));
        FullTest.Add(new Question("",
                               new string[] { "6/9", "6/3", "8/9" },
                               new GameObject[] { secondQuestionGameObject },
                               singleAnswerContainer,
                               2));
        FullTest.Add(new Question("",
                               new string[] { "12/21", "8/7", "16/21" },
                               new GameObject[] { tirdhQuestionGameObject },
                               singleAnswerContainer,
                               2));
        FullTest.Add(new Question("",
                               new string[] { "2/5", "5/6", "1/2" },
                               new GameObject[] { fourthQuestionGameObject },
                               singleAnswerContainer,
                               1));
        FullTest.Add(new Question("",
                               new string[] { "13/30", "7/5", "3/6" },
                               new GameObject[] { fifthQuestionGameObject },
                               singleAnswerContainer,
                               0));
        FullTest.Add(new Question("",
                               new string[] { "11/5", "12/5", "13/5" },
                               new GameObject[] { sixthQuestionGameObject },
                               singleAnswerContainer,
                               1));
        FullTest.Add(new Question("",
                               new string[] { "1/4", "2/3", "6/24" },
                               new GameObject[] { seventhQuestionGameObject },
                               singleAnswerContainer,
                               2));
        FullTest.Add(new Question("",
                               new string[] { "3/3", "3/6", "1/6" },
                               new GameObject[] { eighthtQuestionGameObject },
                               singleAnswerContainer,
                               1));
    }
}
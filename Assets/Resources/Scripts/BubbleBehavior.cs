﻿using System.Collections;
using UnityEngine;

public class BubbleBehavior : MonoBehaviour
{
    public float value;
    public float referenceValue;

    public Reto3 reto3Burbble;
    public Rigidbody2D rigidbody2D;

    // Use this for initialization
    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "bottomCollider")
        {
            if (value.ToString() == referenceValue.ToString())
            {
                reto3Burbble.ErrorMaded();
            }
            Destroy(gameObject);
        }
    }
}
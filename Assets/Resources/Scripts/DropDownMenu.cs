﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DropDownMenu : MonoBehaviour
{
    private GameObject dropDownMenuGameObject;
    private Boolean openMenu;
    private Button toggleMenuButton;
    private Animator menuAnimator;

    private Button closePopUpButton;

    //private GameObject popUpContainterGameObject;
    private GameObject imgFichaTecnicaGameObject, imgCreditosGameObject, imgGlosarioGameObject;

    private Button fichaTecnicaButton, creditosButton, glosarioButton;
    private Button inicioButton;

    private Button indicacionesPanelButton;
    private SoundManager soundManager;

    // Use this for initialization
    private void Start()
    {
        soundManager = transform.GetComponent<SoundManager>();

        dropDownMenuGameObject = GameObject.Find("dropDownMenu");

        toggleMenuButton = dropDownMenuGameObject.transform.GetChild(0).GetComponent<Button>();
        menuAnimator = dropDownMenuGameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
        closePopUpButton = dropDownMenuGameObject.transform.GetChild(1).GetComponent<Button>();

        imgFichaTecnicaGameObject = closePopUpButton.transform.GetChild(0).GetChild(0).gameObject;
        imgCreditosGameObject = closePopUpButton.transform.GetChild(0).GetChild(1).gameObject;
        imgGlosarioGameObject = closePopUpButton.transform.GetChild(0).GetChild(2).gameObject;

        fichaTecnicaButton = menuAnimator.transform.GetChild(0).GetComponent<Button>();
        creditosButton = menuAnimator.transform.GetChild(1).GetComponent<Button>();
        glosarioButton = menuAnimator.transform.GetChild(2).GetComponent<Button>();
        inicioButton = menuAnimator.transform.GetChild(3).GetComponent<Button>();
        indicacionesPanelButton = gameObject.transform.GetChild(6).GetChild(1).GetComponent<Button>();

        toggleMenuButton.onClick.AddListener(ToggleMenu);
        closePopUpButton.onClick.AddListener(ClosePopUps);

        fichaTecnicaButton.onClick.AddListener(delegate ()
        {
            OpenPopUp("Ficha_Tecnica");
        });
        creditosButton.onClick.AddListener(delegate ()
        {
            OpenPopUp("Creditos");
        });
        glosarioButton.onClick.AddListener(delegate ()
        {
            OpenPopUp("Glosario");
        });
        inicioButton.onClick.AddListener(delegate ()
        {
            GoToInicio();
        });
        indicacionesPanelButton.onClick.AddListener(delegate ()
        {
            HidePopUp();
        });
    }

    private void OpenPopUp(string popUpName)
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        closePopUpButton.gameObject.SetActive(true);
        if (popUpName == "Ficha_Tecnica")
        {
            imgFichaTecnicaGameObject.SetActive(true);
        }
        else if (popUpName == "Creditos")
        {
            imgCreditosGameObject.SetActive(true);
        }
        else if (popUpName == "Glosario")
        {
            imgGlosarioGameObject.SetActive(true);
        }
        ToggleMenu();
    }

    private void ClosePopUps()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        imgFichaTecnicaGameObject.SetActive(false);
        imgCreditosGameObject.SetActive(false);
        imgGlosarioGameObject.SetActive(false);
        closePopUpButton.gameObject.SetActive(false);
    }

    private void GoToInicio()
    {
        Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        ToggleMenu();
    }

    private void ToggleMenu()
    {
        openMenu = !openMenu;
        menuAnimator.SetBool("Opened", openMenu);
    }

    private void Update()
    {
        if (Menu.CurrentScenario == Menu.Scenarios.Menu)
        {
            inicioButton.gameObject.SetActive(false);
        }
        else
        {
            inicioButton.gameObject.SetActive(true);
        }
    }

    public void ShowPopUp(Menu.Scenarios scenarios)
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        indicacionesPanelButton.gameObject.SetActive(true);
        foreach (Transform currentReto in indicacionesPanelButton.transform)
        {
            currentReto.gameObject.SetActive(false);
        }

        switch (scenarios)
        {
            case Menu.Scenarios.Reto1:
                indicacionesPanelButton.transform.GetChild(0).gameObject.SetActive(true);

                break;

            case Menu.Scenarios.Reto2:
                print(indicacionesPanelButton.transform.GetChild(0).name);
                indicacionesPanelButton.transform.GetChild(1).gameObject.SetActive(true);

                break;

            case Menu.Scenarios.Reto3:
                indicacionesPanelButton.transform.GetChild(2).gameObject.SetActive(true);

                break;

            case Menu.Scenarios.RetoFinal:

                break;

            default:

                break;
        }
    }

    private void HidePopUp()
    {

        soundManager.playSound("Sound_Cerrar 3", 1);
        Menu.Scenarios currentScenario = Menu.CurrentScenario;

        indicacionesPanelButton.gameObject.SetActive(false);

        switch (currentScenario)
        {
            case Menu.Scenarios.Reto1:
                gameObject.GetComponent<Reto1>().StartReto();
                break;

            case Menu.Scenarios.Reto2:
                gameObject.GetComponent<Reto2>().StartReto();
                break;

            case Menu.Scenarios.Reto3:
                gameObject.GetComponent<Reto3>().StartReto();
                break;

            case Menu.Scenarios.RetoFinal:

                break;

            default:

                break;
        }
    }
}
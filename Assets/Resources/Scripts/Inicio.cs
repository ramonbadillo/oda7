﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Inicio : MonoBehaviour
{
    private GameObject inicioGameObject;
    private GameObject menuGameObject;

    private Button goMenuButton;

    // Use this for initialization
    private void Start()
    {
        inicioGameObject = transform.GetChild(0).gameObject;
        goMenuButton = inicioGameObject.transform.GetChild(1).gameObject.GetComponent<Button>();
        menuGameObject = transform.GetChild(1).gameObject;

        goMenuButton.onClick.AddListener(GoToMainMenu);
    }

    private void GoToMainMenu()
    {
        Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        //inicioGameObject.SetActive(false);
        //menuGameObject.SetActive(true);
    }

    // Update is called once per frame
    private void Update()
    {
    }
}
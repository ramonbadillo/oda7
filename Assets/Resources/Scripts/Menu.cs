﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [HideInInspector]
    public static GameObject MenuGameObject, Reto1GameObject, Reto2GameObject, Reto3GameObject, RetoFinalGameObject, DropDownGameObject,InicioGameObject;

    public static Scenarios CurrentScenario;
    private static GameObject ParentObject;
    private Button reto1Button, reto2Button, reto3Button, retoFinalButton;
    private SoundManager soundManager;

    public enum Scenarios
    {
        Menu,
        Reto1,
        Reto2,
        Reto3,
        Reto4,
        RetoFinal,
    };

    private void Awake()
    {
        ParentObject = gameObject;
        InicioGameObject = transform.GetChild(0).gameObject;
        MenuGameObject = transform.GetChild(1).gameObject;
        Reto1GameObject = transform.GetChild(2).gameObject;
        Reto2GameObject = transform.GetChild(3).gameObject;
        Reto3GameObject = transform.GetChild(4).gameObject;
        RetoFinalGameObject = transform.GetChild(5).gameObject;
        DropDownGameObject = transform.GetChild(6).gameObject;

        InicioGameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        MenuGameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        Reto1GameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        Reto2GameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        InicioGameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        Reto3GameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        RetoFinalGameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        DropDownGameObject.GetComponent<Canvas>().worldCamera = Camera.main;

    }

    // Use this for initialization
    private void Start()
    {
        soundManager = transform.GetComponent<SoundManager>();
        soundManager.playSound("Tropical Jazz Paradise", 0);
        reto1Button = MenuGameObject.transform.GetChild(0).GetComponent<Button>();
        reto2Button = MenuGameObject.transform.GetChild(1).GetComponent<Button>();
        reto3Button = MenuGameObject.transform.GetChild(2).GetComponent<Button>();
        retoFinalButton = MenuGameObject.transform.GetChild(3).GetComponent<Button>();


        reto1Button.onClick.AddListener(delegate ()
        {
            GoToTheNextScenarioButtonMethod(Scenarios.Reto1);
            gameObject.GetComponent<DropDownMenu>().ShowPopUp(Scenarios.Reto1);
        });
        reto2Button.onClick.AddListener(delegate ()
        {
            GoToTheNextScenarioButtonMethod(Scenarios.Reto2);
            gameObject.GetComponent<DropDownMenu>().ShowPopUp(Scenarios.Reto2);
        });
        reto3Button.onClick.AddListener(delegate ()
        {
            GoToTheNextScenarioButtonMethod(Scenarios.Reto3);
            gameObject.GetComponent<DropDownMenu>().ShowPopUp(Scenarios.Reto3);
        });
        retoFinalButton.onClick.AddListener(delegate ()
        {
            GoToTheNextScenarioButtonMethod(Scenarios.RetoFinal);
            gameObject.GetComponent<RetoFinal>().StartReto();
        });
    }

    private void GoToTheNextScenarioButtonMethod(Scenarios scenarios)
    {
        
        GoToScenario(scenarios);

    }

    public static void GoToScenarioStaticMethod(Scenarios scenarios)
    {
        GoToScenario(scenarios);
    }

    private static void GoToScenario(Scenarios scenarios)
    {
        CurrentScenario = scenarios;

        switch (scenarios)
        {
            case Scenarios.Menu:
                MenuGameObject.SetActive(true);
                HideScenarios(1);
                break;

            case Scenarios.Reto1:
                Reto1GameObject.SetActive(true);
                HideScenarios(2);
                break;

            case Scenarios.Reto2:
                Reto2GameObject.SetActive(true);
                HideScenarios(3);
                break;

            case Scenarios.Reto3:
                Reto3GameObject.SetActive(true);
                HideScenarios(4);
                break;

            case Scenarios.RetoFinal:
                RetoFinalGameObject.SetActive(true);
                HideScenarios(5);
                break;
        }
    }

    private static void HideScenarios(int indexUnhiddenScenario)
    {
        for (int i = 0; i < ParentObject.transform.childCount - 1; i++)
        {
            if (indexUnhiddenScenario != i)
            {
                ParentObject.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        DropDownGameObject.SetActive(true);
    }
}
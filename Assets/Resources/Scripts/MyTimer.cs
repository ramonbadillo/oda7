﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MyTimer : MonoBehaviour
{
    [HideInInspector]
    public string timerText;

    [HideInInspector]
    public float secondsCount;

    private float stopTime;

    [HideInInspector]
    public float remainingTime = 0;

    private bool timerEnable = false;

    private bool timerWatchEnable = false;

    private void Update()
    {
        UpdateTimerUI();
    }

    //call this on update
    public void UpdateTimerUI()
    {
        if (timerWatchEnable)
        {
            secondsCount += Time.deltaTime;
            timerText = FormatTime((int)secondsCount);
        }
        else if (timerEnable)
        {
            secondsCount += Time.deltaTime;
            remainingTime = stopTime - secondsCount;
            //Debug.Log(remainingTime);
            timerText = FormatTime((int)remainingTime);
        }
        if (0 >= remainingTime)
        {
            StopTimer();
        }
    }

    public void ResetMyTimer()
    {
        StopTimeWatch();
        StopTimer();
        remainingTime = 0;
        secondsCount = 0;
    }

    public string FormatTime(int seconds)
    {
        TimeSpan t = TimeSpan.FromSeconds(seconds);
        string result = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
        return result;
    }

    public void StartTimer(float stopTime)
    {
        this.stopTime = stopTime;
        timerEnable = true;
    }

    public void StopTimer()
    {
        timerEnable = false;
    }

    public void StopTimeWatch()
    {
        timerWatchEnable = false;
    }

    public void StartTimeWatch()
    {
        timerWatchEnable = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

public class Reto2 : MonoBehaviour
{
    public readonly int scoreToWin = 6;

    private List<Toggle> cardsArrayToggles;
    private GameObject cardsContaGameObject;
    private Text timerText;
    private Button pauseButton, restartButton;

    private Toggle oneFractionTogglePrefab, twoFractionTogglePrefab;

    private List<string> optionsBank = new List<string>();
    private List<GameObject> cardsGameObjects = new List<GameObject>();
    private GameObject overlayButtonsContainer;
    private GameObject popUpElements, pausePopUp;
    private Button exitButtonPause, continueButtonPause, winButton;

    private float firstValue = 0;
    private float secondValue = 0;

    private Toggle firstToggle, secondToggle;

    private Button cancelButton, aceptButton;

    private int score = 0;

    private MyTimer myTimer;

    private SoundManager soundManager;

    private struct Fraction
    {
        public string numerador;
        public string denominador;
    };

    // Use this for initialization
    private void Start()
    {
        //Get all the Toggles
        soundManager = transform.GetComponent<SoundManager>();
        timerText = Menu.Reto2GameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        cardsContaGameObject = Menu.Reto2GameObject.transform.GetChild(1).GetChild(0).gameObject;
        overlayButtonsContainer = Menu.Reto2GameObject.transform.GetChild(1).GetChild(1).gameObject;
        restartButton = Menu.Reto2GameObject.transform.GetChild(2).GetComponent<Button>();
        pauseButton = Menu.Reto2GameObject.transform.GetChild(3).GetComponent<Button>();
        popUpElements = Menu.Reto2GameObject.transform.GetChild(4).gameObject;
        pausePopUp = popUpElements.transform.GetChild(0).gameObject;
        winButton = popUpElements.transform.GetChild(1).GetComponent<Button>();
        exitButtonPause = pausePopUp.transform.GetChild(0).GetComponent<Button>();
        continueButtonPause = pausePopUp.transform.GetChild(1).GetComponent<Button>();
        oneFractionTogglePrefab = Menu.Reto2GameObject.transform.GetChild(1).GetChild(2).gameObject.GetComponent<Toggle>();
        twoFractionTogglePrefab = Menu.Reto2GameObject.transform.GetChild(1).GetChild(3).gameObject.GetComponent<Toggle>();

        aceptButton = overlayButtonsContainer.transform.GetChild(0).gameObject.GetComponent<Button>();
        cancelButton = overlayButtonsContainer.transform.GetChild(1).gameObject.GetComponent<Button>();

        myTimer = gameObject.GetComponent<MyTimer>();

        winButton.onClick.AddListener(delegate ()
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto3);
            gameObject.GetComponent<Reto3>().StartReto();
            Time.timeScale = 1.0f;
        });

        exitButtonPause.onClick.AddListener(delegate ()
        {
            ExitGame();
        });

        continueButtonPause.onClick.AddListener(delegate ()
        {
            ResumeGame();
        });

        pauseButton.onClick.AddListener(delegate ()
        {
            PauseGame();
        });

        restartButton.onClick.AddListener(delegate ()
        {
            StartReto();
        });

        aceptButton.onClick.AddListener(delegate ()
        {
            AcceptAction();
        });

        cancelButton.onClick.AddListener(delegate ()
        {
            CancelAction();
        });
        //StartReto();
    }

    private void ClearReto()
    {
        foreach (Transform currentPopUp in popUpElements.transform)
        {
            currentPopUp.gameObject.SetActive(false);
        }
        foreach (Transform cartas in cardsContaGameObject.transform)
        {
            Destroy(cartas.gameObject);
        }
        List<string> randomList = RandomList(optionsBank, 6);
        foreach (string currentString in randomList)
        {
            SetFractionsToGameObject(ToFraction(currentString));
        }
        popUpElements.SetActive(false);
        Menu.DropDownGameObject.SetActive(true);
        SetPositions(cardsGameObjects);
    }

    public void StartReto()
    {
        score = 0;
        FillBankList();
        Time.timeScale = 1.0f;
        ClearReto();

        CancelAction();
        myTimer.ResetMyTimer();
        myTimer.StartTimeWatch();
    }

    private bool userAction = true;

    public void TurnCard(bool value, GameObject fractionToggle, float number)
    {
        fractionToggle.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(value);
        if (userAction)
        {
            if (firstValue == 0)
            {
                firstValue = number;
                firstToggle = fractionToggle.GetComponent<Toggle>();
                firstToggle.interactable = false;
            }
            else
            {
                secondValue = number;
                secondToggle = fractionToggle.GetComponent<Toggle>();
                secondToggle.interactable = false;
                overlayButtonsContainer.SetActive(true);
            }
        }
    }

    private List<string> RandomList(List<string> optionsList, int maxElements)
    {
        List<string> randomStringList = new List<string>();
        int randomIndex = Random.Range(0, optionsList.Count);
        for (int i = 0; i < maxElements; i++)
        {
            randomStringList.Add(optionsList[randomIndex]);
            optionsList.RemoveAt(randomIndex);
            randomIndex = Random.Range(0, optionsList.Count);
        }
        return randomStringList;
    }

    private void CancelAction()
    {
        userAction = false;
        if (firstToggle != null)
        {
            firstToggle.isOn = false;
            firstToggle.interactable = true;
        }
        if (secondToggle != null)
        {
            secondToggle.isOn = false;
            secondToggle.interactable = true;
        }

        overlayButtonsContainer.SetActive(false);
        userAction = true;
        
        
        firstToggle = null;
        secondToggle = null;

        firstValue = 0;
        secondValue = 0;
    }

    private void AcceptAction()
    {
        if (firstValue.ToString() == secondValue.ToString())
        {
            soundManager.playSound("Sound_correcto 10", 1);
            firstToggle.interactable = false;
            secondToggle.interactable = false;
            overlayButtonsContainer.SetActive(false);
            firstToggle = null;
            secondToggle = null;

            firstValue = 0;
            secondValue = 0;
            score++;
        }
        else
        {
            soundManager.playSound("Sound_incorrecto 19", 1);
            CancelAction();
        }
    }

    private void WinGame()
    {
        myTimer.StopTimeWatch();
        soundManager.playSound("TerminarReto", 1);
        popUpElements.SetActive(true);
        winButton.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (scoreToWin <= score)
        {
            WinGame();
            
            if (PlayerPrefs.HasKey("ODA7_Reto2_minTime"))
            {
                PlayerPrefs.SetFloat("ODA7_Reto2_minTime", myTimer.secondsCount);
            }
            else
            {
                if (PlayerPrefs.GetFloat("ODA7_Reto2_minTime") < myTimer.secondsCount)
                {
                    PlayerPrefs.SetFloat("ODA7_Reto2_minTime", myTimer.secondsCount);
                }
            }
        }
        else
        {
            timerText.text = myTimer.timerText;
        }
    }

    private List<Fraction> ToFraction(string stringFraction)
    {
        List<Fraction> fractions = new List<Fraction>();
        char[] delimiterChars = { '+', '=' };
        string[] splitFractions = stringFraction.Split(delimiterChars);
        foreach (string currentFraction in splitFractions)
        {
            //Debug.Log(currentFraction);
            Fraction newFraction;
            string[] numbers = currentFraction.Split('/');
            newFraction.numerador = numbers[0];
            newFraction.denominador = numbers[1];
            fractions.Add(newFraction);
        }

        return fractions;
    }

    private void SetFractionsToGameObject(List<Fraction> fractionsList)
    {
        if (fractionsList.Count == 3)
        {
            GameObject newTwoFractionGameObject = Instantiate(this.twoFractionTogglePrefab.gameObject);
            cardsGameObjects.Add(newTwoFractionGameObject);
            newTwoFractionGameObject.transform.SetParent(cardsContaGameObject.transform);
            newTwoFractionGameObject.transform.localScale = Vector3.one;
            GameObject fractionContainer = newTwoFractionGameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;

            fractionContainer.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text =
                fractionsList[0].numerador;
            fractionContainer.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>().text =
                fractionsList[0].denominador;

            fractionContainer.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text =
                fractionsList[1].numerador;
            fractionContainer.transform.GetChild(1).GetChild(1).gameObject.GetComponent<Text>().text =
                fractionsList[1].denominador;
            newTwoFractionGameObject.SetActive(true);
            Toggle twoFractionToggle = newTwoFractionGameObject.GetComponent<Toggle>();
            float result = FractionToNumber(fractionsList[0]) + FractionToNumber(fractionsList[1]);
            twoFractionToggle.onValueChanged.AddListener(delegate
            {
                TurnCard(twoFractionToggle.isOn, twoFractionToggle.gameObject, result);
            });

            GameObject newOneFractionGameObject = Instantiate(this.oneFractionTogglePrefab.gameObject);
            cardsGameObjects.Add(newOneFractionGameObject);
            newOneFractionGameObject.transform.SetParent(cardsContaGameObject.transform);
            newOneFractionGameObject.transform.localScale = Vector3.one;

            fractionContainer = newOneFractionGameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;

            fractionContainer.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text =
                fractionsList[2].numerador;
            fractionContainer.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>().text =
                fractionsList[2].denominador;

            newOneFractionGameObject.SetActive(true);

            Toggle oneFractionToggle = newOneFractionGameObject.GetComponent<Toggle>();

            oneFractionToggle.onValueChanged.AddListener(delegate
            {
                TurnCard(oneFractionToggle.isOn, oneFractionToggle.gameObject, FractionToNumber(fractionsList[2]));
            });
        }
    }

    private void SetPositions(List<GameObject> cardsGameObjects)
    {
        int randomIndex = Random.Range(0, cardsGameObjects.Count);
        float width = cardsContaGameObject.GetComponent<RectTransform>().rect.width / 2;
        float height = cardsContaGameObject.GetComponent<RectTransform>().rect.height / 2;

        float[] xPositions = new[] { -width / 2, 0, width / 2 };
        float[] yPositions = new[] { -(2 * height) / 3, -64, 64, (2 * height) / 3 };

        for (int x = 0; x < xPositions.Length; x++)
        {
            for (int y = 0; y < yPositions.Length; y++)
            {
                if (cardsGameObjects.Count != 0)
                {
                    cardsGameObjects[randomIndex].gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(xPositions[x], yPositions[y]);
                    cardsGameObjects.RemoveAt(randomIndex);
                    randomIndex = Random.Range(0, cardsGameObjects.Count);
                }
            }
        }
    }

    private float FractionToNumber(Fraction fraction)
    {
        float numerador = int.Parse(fraction.numerador);
        float denominador = int.Parse(fraction.denominador);
        return numerador / denominador;
    }

    private void FillBankList()
    {
        optionsBank.Add("2/3+4/3=6/3");
        optionsBank.Add("7/3+6/3=13/3");
        optionsBank.Add("5/5+9/5=14/5");
        optionsBank.Add("8/2+6/2=14/2");
        optionsBank.Add("3/2+6/2=9/2");
        optionsBank.Add("4/3+8/3=12/3");
        optionsBank.Add("1/3+4/3=5/3");
        optionsBank.Add("7/6+5/6=12/6");
        optionsBank.Add("4/5+3/5=7/5");
        optionsBank.Add("4/2+6/2=10/2");
        optionsBank.Add("3/7+1/7=4/7");//Mal
        optionsBank.Add("1/2+1/2=2/2");
        optionsBank.Add("9/5+8/5=17/5");
        optionsBank.Add("6/2+9/2=15/2");
        optionsBank.Add("4/8+3/8=7/8");
        optionsBank.Add("6/8+2/8=8/8");
        optionsBank.Add("4/9+6/9=10/9");
        optionsBank.Add("4/6+5/6=9/6");
        optionsBank.Add("2/5+3/5=5/5");
        optionsBank.Add("7/5+2/5=9/5");//Mal
        optionsBank.Add("6/9+3/9=9/9");
        optionsBank.Add("8/7+9/7=17/7");
        optionsBank.Add("5/2+3/2=8/2");
        optionsBank.Add("2/7+6/7=8/7");
    }

    private void ResumeGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        Time.timeScale = 1.0f;
        popUpElements.SetActive(false);
        pausePopUp.SetActive(false);
    }

    private void ExitGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        Menu.DropDownGameObject.SetActive(true);
        Time.timeScale = 1.0f;
    }

    private void PauseGame()
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        Time.timeScale = 0.0f;
        popUpElements.SetActive(true);
        pausePopUp.SetActive(true);
    }
}
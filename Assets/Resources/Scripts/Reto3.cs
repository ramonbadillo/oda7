﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Reto3 : MonoBehaviour
{
    public readonly int scoreToWin = 30;
	public readonly float timeToBlink = 10.0f;

    private GameObject bubbleGameObjectPrefab;
    private GameObject bubbleContainer;
    private GameObject popUpElements, pausePopUp, failPopUp, winPopUp;
    private Button exitButtonPause, continueButtonPause, exitButtonFail, restartButtonFail, pauseButton, winButton;

    private Text timerText, scoreText, failText;
    private MyTimer myTimer;

    private float currentValue = 0.5f;

    private float enlapsedTime;

    private int maxSpawnTime = 2;

    private bool startedGame = false;

    private Image backgroundImage;
    private GameObject currentFractionGameObject;
    private Image currentFractionImage;

    private SoundManager soundManager;
    private GameObject gotasTristesContainer;
    private int numberErrors = 0, maxNumberErrors = 3;

    private struct Fraction
    {
        public string numerador;
        public string denominador;
    };

    // Use this for initialization
    private void Start()
    {

        soundManager = transform.GetComponent<SoundManager>();
        bubbleContainer = Menu.Reto3GameObject.transform.GetChild(6).gameObject;
        bubbleGameObjectPrefab = Menu.Reto3GameObject.transform.GetChild(4).gameObject;
        pauseButton = Menu.Reto3GameObject.transform.GetChild(3).gameObject.GetComponent<Button>();
        timerText = Menu.Reto3GameObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        scoreText = Menu.Reto3GameObject.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Text>();
        gotasTristesContainer = Menu.Reto3GameObject.transform.GetChild(2).gameObject;
        backgroundImage = Menu.Reto3GameObject.GetComponent<Image>();
        myTimer = gameObject.GetComponent<MyTimer>();
        currentFractionGameObject = Menu.Reto3GameObject.transform.GetChild(5).gameObject;
        currentFractionImage = currentFractionGameObject.GetComponent<Image>();
        popUpElements = Menu.Reto3GameObject.transform.GetChild(8).gameObject;
        pausePopUp = popUpElements.transform.GetChild(0).gameObject;
        exitButtonPause = pausePopUp.transform.GetChild(0).GetComponent<Button>();
        continueButtonPause = pausePopUp.transform.GetChild(1).GetComponent<Button>();
        failPopUp = popUpElements.transform.GetChild(1).gameObject;
        restartButtonFail = failPopUp.transform.GetChild(0).GetComponent<Button>();
        exitButtonFail = failPopUp.transform.GetChild(1).GetComponent<Button>();
        failText = failPopUp.transform.GetChild(2).GetComponent<Text>();
        winButton = popUpElements.transform.GetChild(2).gameObject.GetComponent<Button>();
        winPopUp = popUpElements.transform.GetChild(2).gameObject;

        winButton.onClick.AddListener(delegate ()
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.RetoFinal);
            gameObject.GetComponent<RetoFinal>().StartReto();
        });

        restartButtonFail.onClick.AddListener(delegate ()
        {
            soundManager.playSound("Sound_Cerrar 3", 1);
            StartReto();
        });

        exitButtonFail.onClick.AddListener(delegate ()
        {
            ExitGame();
        });

        exitButtonPause.onClick.AddListener(delegate ()
        {
            ExitGame();
        });

        continueButtonPause.onClick.AddListener(delegate ()
        {
            ResumeGame();
        });

        pauseButton.onClick.AddListener(delegate ()
        {
            PauseGame();
        });
    }

    private void ClearReto()
    {
        Score = 0;
        scoreText.text = "" + Score;
        numberErrors = 0;
        foreach (Transform currentBubble in bubbleContainer.transform)
        {
            Destroy(currentBubble.gameObject);
        }

        foreach (Transform currentGota in gotasTristesContainer.transform)
        {
            currentGota.gameObject.SetActive(false);
        }

        foreach (Transform currentPopUp in popUpElements.transform)
        {
            currentPopUp.gameObject.SetActive(false);
        }
        popUpElements.SetActive(false);
        Menu.DropDownGameObject.SetActive(true);
    }

    public void StartReto()
    {

        Time.timeScale = 1.0f;
        ClearReto();
        animStop = true;
        FillBankList();
        backgroundImage.GetComponent<Animator>().Play("default");
        Fraction currentFraction = goodOptionsFractions[Random.Range(0, goodOptionsFractions.Count)];
        currentValue = float.Parse(currentFraction.numerador) / float.Parse(currentFraction.denominador);
        SetCurrentFraction(currentFraction);

        myTimer.ResetMyTimer();
        myTimer.StartTimer(61.0f);
        startedGame = true;
    }

    private void SetCurrentFraction(Fraction f)
    {
        currentFractionGameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = f.numerador;
        currentFractionGameObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = f.denominador;
    }

    private int randomFractionIndex;
    private bool animStop = true;

    private void Update()
    {
        if (startedGame)
        {
            enlapsedTime += Time.deltaTime;
            if (maxSpawnTime < enlapsedTime)
            {
                randomFractionIndex = Random.Range(0, 3);
                if (0 == randomFractionIndex)
                {
                    SpawnBubble(goodOptionsFractions[Random.Range(0, goodOptionsFractions.Count)]);
                }
                else
                {
                    SpawnBubble(badOptionsFractions[Random.Range(0, badOptionsFractions.Count)]);
                }
                enlapsedTime = 0.0f;
            }
            timerText.text = myTimer.timerText;

			if (myTimer.remainingTime <= timeToBlink && animStop)
            {
                animStop = false;
                backgroundImage.GetComponent<Animator>().Play("BackgroundAnim");
            }
            if (myTimer.remainingTime <= 0.0f)
            {
                LoseGame();
                startedGame = false;
            }
        }
    }

    private void SpawnBubble(Fraction fraction)
    {
        GameObject newBubble = Instantiate(bubbleGameObjectPrefab);
        newBubble.transform.SetParent(bubbleContainer.transform);
        newBubble.transform.localScale = Vector3.one;
        float xPosition = Random.Range(-412, 552);
        float yPosition = 451;

        newBubble.transform.GetChild(0).GetComponent<Text>().text = fraction.numerador;
        newBubble.transform.GetChild(1).GetComponent<Text>().text = fraction.denominador;

        newBubble.GetComponent<RectTransform>().transform.localPosition = new Vector3(xPosition, yPosition);

        newBubble.AddComponent<BubbleBehavior>();
        newBubble.GetComponent<BubbleBehavior>().value = float.Parse(fraction.numerador) / float.Parse(fraction.denominador);
        newBubble.GetComponent<BubbleBehavior>().referenceValue = currentValue;
        newBubble.GetComponent<BubbleBehavior>().reto3Burbble = this;
        Button bubbleButton = newBubble.GetComponent<Button>();
        bubbleButton.onClick.AddListener(delegate ()
        {
            BubbleButtonAction(newBubble, FractionToNumber(fraction));
        });
        newBubble.SetActive(true);
    }

    private int Score = 0;

    private void BubbleButtonAction(GameObject bubbleGameObject, float fractionResult)
    {
        bubbleGameObject.GetComponent<Animator>().Play("Destroy");

        if (bubbleGameObject.GetComponent<BubbleBehavior>().value.ToString() == currentValue.ToString())
        {
            soundManager.playSound("Sound_correcto 10", 1);
            Score += 5;
            myTimer.secondsCount -= 5;
            scoreText.text = "" + Score;
        }
        else
        {
            soundManager.playSound("Sound_incorrecto 19", 1);
            ErrorMaded();
        }

        if (Score >= scoreToWin)
        {
            WinGame();
        }
        Destroy(bubbleGameObject, 1.0f);
    }

    private float FractionToNumber(Fraction fraction)
    {
        float numerador = int.Parse(fraction.numerador);
        float denominador = int.Parse(fraction.denominador);
        return numerador / denominador;
    }

    private List<Fraction> goodOptionsFractions = new List<Fraction>();
    private List<Fraction> badOptionsFractions = new List<Fraction>();
    private List<string> optionsBank = new List<string>();

    private void FillBankList()
    {
        ClearLists();
        optionsBank.Add("1/2,2/4,3/6,4/8,5/10,6/12");
        optionsBank.Add("1/3,2/6,3/9,4/12,5/15");
        optionsBank.Add("1/4,2/8,3/12,4/16,5/20");
        optionsBank.Add("1/5,2/10,3/15,4/20,5/25");
        optionsBank.Add("2/5,4/10,16/15,8/20,10/25");

        int randomIndex = Random.Range(0, optionsBank.Count);
        goodOptionsFractions = FillFractionList(optionsBank[randomIndex]);
        optionsBank.RemoveAt(randomIndex);

        string badOptionsString = "";

        badOptionsString += optionsBank[0];
        for (int i = 1; i < optionsBank.Count; i++)
        {
            badOptionsString += "," + optionsBank[i];
        }

        badOptionsFractions = FillFractionList(badOptionsString);
    }

    private List<Fraction> FillFractionList(string options)
    {
        List<Fraction> fractionList = new List<Fraction>();

        string[] fractions = options.Split(',');
        foreach (string currentFraction in fractions)
        {
            string[] fraction = currentFraction.Split('/');
            Fraction f = new Fraction();
            f.numerador = fraction[0];
            f.denominador = fraction[1];
            fractionList.Add(f);
        }
        return fractionList;
    }

    private void ClearLists()
    {
        goodOptionsFractions.Clear();
        badOptionsFractions.Clear();
        optionsBank.Clear();
    }

    private void ResumeGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        Time.timeScale = 1.0f;
        popUpElements.SetActive(false);
        pausePopUp.SetActive(false);
    }

    private void ExitGame()
    {
        soundManager.playSound("Sound_Cerrar 3", 1);
        startedGame = false;
        Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        Menu.DropDownGameObject.SetActive(true);
        Time.timeScale = 1.0f;
    }

    private void PauseGame()
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);

        Time.timeScale = 0.0f;
        popUpElements.SetActive(true);
        pausePopUp.SetActive(true);
    }

    private void WinGame()
    {
        soundManager.playSound("TerminarReto", 1);
        popUpElements.SetActive(true);
        winPopUp.SetActive(true);
        Time.timeScale = 0.0f;
        startedGame = false;
    }

    private void LoseGame()
    {
        soundManager.playSound("Sound_OpenPopUp 10", 1);
        failText.text = "Excelente, conseguiste " + Score + " puntos.";

        Time.timeScale = 0.0f;
        Menu.DropDownGameObject.SetActive(false);
        popUpElements.SetActive(true);
        failPopUp.SetActive(true);
        startedGame = false;
    }

    public void ErrorMaded()
    {
        
        if (maxNumberErrors > (numberErrors + 1))
        {
            gotasTristesContainer.transform.GetChild(numberErrors).gameObject.SetActive(true);
            numberErrors++;
        }
        else
        {
            gotasTristesContainer.transform.GetChild(numberErrors).gameObject.SetActive(true);
            LoseGame();
        }
    }
}